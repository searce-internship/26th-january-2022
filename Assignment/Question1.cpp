#include<iostream>
#include<vector>
#include<sstream>
#include<algorithm>

using namespace std;

bool sortbySecondElement(const pair<int, int>& x,
const pair<int, int>& y)
{
  return(x.second < y.second);
}

int main()
{
  pair<int, int> data;
  vector<pair<int, int> > V;
  int n;
  cout<<"Enter the customers: ";
  cin>>n;
  cin.ignore();
  int temp_data[2];
  for(size_t i =0; i<n ; i++)
  {
    for(size_t i=0; i<2; i++)
    {
      cin>>temp_data[i];
    }
    data.first=temp_data[0];
    data.second = temp_data[1];
    V.push_back(data);
  }
  sort(V.begin(), V.end(), sortbySecondElement);
  int avg_sum=0;
  int sum=0;
  for(size_t i =0; i<n;i++)
  {
    sum+=V[i].second;
    avg_sum+=sum-V[i].first;
  }
  cout<<(avg_sum/n);
}